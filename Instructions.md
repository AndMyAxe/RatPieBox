# Instructions to set up your own Rat Pie Box

I am doing this from a computer running Linux Mint, hopefully others can add
whatever is needed specific to other platforms.

If there are any parts of this that need more explanation let me know so I can
add it. We should probably add screenshots for most parts.

## Setup raspberry pi and harddrive

1. Get a raspberry pi. I am currently setting up a pi 3, but I hope that most
  of this will work on a pi zero w as well. Other versions of a raspberry pi
  will need some sort of wireless adapter.
2. Get a microSD card. An 8 gig class 10 card is probably the best choice. It
  is large enough and is the cheapest option. A smaller card may be large
  enough but they often cost just as much as the 8 gig cards.
3. Download the newest raspbian image from here:
  https://www.raspberrypi.org/downloads/raspbian/
  (Using NOOBS is a better idea maybe?)
  (Ubuntu mate would probably also work, and possibly others. we should look
  into this to figure out which one is best.)
4. Unzip the downloaded archive, it will give you a folder with a name like NOOBS-
5. Now we need to clone the image onto the sd card:
  - Linux: I used USB Image Writer, you just open the application, pick where
  you have the .img file as the write image and select the microsd card as the
  to option and click `Write`.
6. Put the microSD card into the raspberry pi, connect it to a monitor keyboard and mouse and start it up.
7. Connect the hard drive and reboot the pi
8. Setup wifi, install raspbian. Change the hostname to `PieServer`
9. Boot up the pi
10. add the harddrive to `/etc/fstab` so you don't have to manually mount it.
  - First get the UUID or your harddrive. Type `sudo blkid -s UUID` in the terminal.
    most lines will start with something like `/dev/mmcblkxxxx` followed by
    some other things.
    One line (probably the last one) should look like this:
    `/dev/sda1: UUID="0746665d-16d4-44ec-86de-ca89bf77c8d9"` you need to UUID
    for this line so write it down or copy it somewhere.
  - Add the harddrive to `/etc/fstab` by typing `sudo nano /etc/fstab` at the
    end of the file add the line `UUID=XXXXXXX /media/PieTin ext4 defaults 0 0`
    where you replace XXXXXXX with the UUID of the hard drive.
11. Generate locale `raspi-config`, then select `Localisation options`, select
  the one you want (`en-GB` or `en-US` for British or American english)
12. Change the password for the pi. `passwd` then type the current password
  `raspberry` then when asked enter the new password. If done successfully the
  terminal will say `passwd: password updated successfully`.
  Make sure you remember the password or you won't be able to log into the pi
  again.

## Remove stuff we don't need (optional)
- Remove libreoffice (running headless, so not useful) `sudo apt-get remove libreoffice*`
- remove idle (python gui) `sudo apt-get remove idle*`
- remove epiphany browser `sudo apt-get remove epiphany*`
- remove wolfram alpha `sudo apt-get remove wolfram*`
- remove scratch `sudo apt-get remove scratch*`
- remove minecraft `sudo apt-get remove minecraft-pi`
- remove chromium browser `sudo apt-get remove chromium-browser`
- clean up `sudo apt autoremove`

## Setup wifi and access point

Set up the pi as an access point and let it connect to wifi.
Some of this may be redundant, but this is how I got it to work consistently

1. Install stuff!
  `sudo apt-get install dnsmasq hostapd`
2. `sudo nano /etc/dnsmasq.conf`
  - Put this into the file:
  ```
    interface=lo,ap0
    no-dhcp-interface=lo,wlan0
    bind-interfaces
    server=8.8.8.8
    domain-needed
    bogus-priv
    dhcp-range=192.168.10.50,192.168.10.150,255.255.255.0,24h
  ```
  save and exit with:
  `ctrl+x`, `y`, `enter`
3. `sudo nano /etc/hostapd/hostapd.conf`
  - Put this in the file:
  ```
    ctrl_interface=/var/run/hostapd
    ctrl_interface_group=0
    interface=ap0
    driver=nl80211
    ssid=PieServer
    hw_mode=g
    channel=7
    wmm_enabled=0
    macaddr_acl=0
    auth_algs=1
    wpa=2
    wpa_passphrase=RatPieBox
    wpa_key_mgmt=WPA-PSK
    wpa_pairwise=TKIP
    rsn_pairwise=CCMP
  ```
  save and exit with:
  `ctrl+x`, `y`, `enter`
4. In the file `/etc/default/hostapd` change the line `#DAEMON_CONF` to
  `DAEMON_CONF="/etc/hostapd/hostapd.conf"`
  - `sudo nano /etc/hostapd/hostapd`
5. `sudo nano /etc/wpa_supplicant/wpa_supplicant.conf`
  - put this in the file:
  ```
    ctrl_interface=DIR=/var/run/wpa_supplicant GROUP=netdev

    network={
      ssid="PutYourSSIDHere"
      psk="ThePassword"
      id_str="AP1"
    }
  ```
  put the ssid and password in place of PutYourSSIDHerer and ThePassword
6. Get your mac address by typing `iw dev` in the terminal. The mac address
    in on the line that looks like this `addr b8:27:eb:ff:ff:ff` and is the
    `b8:27:eb:ff:ff:ff` part.
7. `sudo nano /etc/network/interfaces`
  - in the file you want these things (replace all places with
  `ff:ff:ff:ff:ff:ff` with your mac address from above):
  ```
    allow-hotplug wlan0
    iface wlan0 inet manual
      pre-up ifup ap0
      wpa-roam /etc/wpa_supplicant/wpa_supplicant.conf
      pre-down ifdown ap0
    iface AP1 inet dhcp

    allow-hotplug ap0
    iface ap0 inet static
      pre-up iw phy phy0 interface add ap0 type __ap0
      pre-up ip link set ap0 address ff:ff:ff:ff:ff:ff
      post-down iw dev ap0 del
      address 192.168.10.1
      netmask 255.255.255.0
      hostapd /etc/hostapd/hostapd.conf
  ```
  save and exit with:
  `ctrl+x`, `y`, `enter`
7. Edit rc.local to make everything work consistently and to start up Gitea on
    boot.
    - `sudo nano /etc/rc.local`
    - Add this to the file before the line `exit 0` at the end:
    ```
    # Make the wireless interface to work consistently
    # Wait for the boot to finish
    sleep 5
    # Reset the wlan interface. I don't know why this is necessary.
    sudo ifdown --force wlan0
    sudo ifup wlan0
    # start Gitea
    su -c `/media/PieTin/Gitea/gitea web > /dev/null 2>&1 &` - gitea
    # start TiddlyWiki
    # Only uncomment this if you have tiddlywiki installed and setup
    /media/PieTin/TiddlyWiki/start.sh &
    # start the express server
    node /media/PieTin/Server/express-server.js &
    ```
    - This starts the gitea process as the user gitea and ignores the output
    but not errors.
8. This wasn't working consistently on my linux computer but it was working
  fine on a laptop running osX. To allow the linux desktop to consistently
  connect I added a cron job that resets the avahi-daemon on the rat pie box.
  - In the terminal `crontab -e`
  - At the bottom of the file add the line `*/1 * * * * /bin/bash /media/PieTin/Server/resetwifi.sh >> /media/PieTin/errors.txt 2>&1`
  - The `resetwifi.sh` file is currently in the `RatPieBoxServer` repo.
9. There were also some changes to the file `/etc/avahi/avahi-daemon.conf` that
  may have some effect but I don't think so. I need to look into that.
10. In theory if you reboot now you should be able to see a wireless access
  point called PieServer on other devices, and the pi will be connected to your
  local wifi. If you type `ping pieserver.local` on a linux machine you should
  be able to see it.

## Install MySQL and Gitea

11. Install mysql
  - `sudo apt-get install mysql-server`
12. Set up the mysql db and user
  - `sudo mysql -u root`
  - `GRANT ALL PRIVILEGES ON *.* TO 'gitea'@'localhost' IDENTIFIED BY 'password';`
    - Replace `password` with the password you want to use.
  - `CREATE DATABASE gitea;`
13. Get Gitea
14. Setup Gitea (default settings work with the above stuff)
  - Create a gitea user to run the application or things will be annoying
    - `sudo adduser gitea` (I set password to `gitea`, probably a bad idea.)
  - You need to edit `/media/PieTin/Gitea/custom/conf/app.ini` and change:
    - line `ROOT = /home/pi/gitea-repositories` to
      `ROOT = /media/PieTin/Gitea/gitea-repositories`.
    - You can also change the line `APP_NAME = Gitea: Git with a cup of tea` to
      whatever name you want.
    - `RUN_USER = pi` to `RUN_USER = gitea`
    - You also need to set the `gitea` user as the owner of the PieTin, so
      `chown -R gitea /media/PieTin`
15. Create an RSA key to use for ssh for the gitea user. (work in progress!)
  - `su - gitea`
  - `ssh-keygen -t rsa -b 4096 -C "youremail@yourmail.com"`
  where you replace `youremail@yourmail.com` with your email address.
  - You then have to put the public key on whatever serves you want to be able
  to push to in your git hooks. (like on gitlab or github)
  - Get the public key `cat ~/.ssh/id_rsa.pub` (instructions about how to copy
    it and which part is important)
  - Add this key to your gitea account (need instrucitons here and more detal!)
  - To push to a new host you may need to manually do at least one push to it
  on the command line to add the host to the known_hosts on the RatPieBox.

## Installing Nodejs

Instructions from here: https://www.w3schools.com/nodejs/nodejs_raspberrypi.asp

1. `curl -sL https://deb.nodesource.com/setup_8.x | sudo -E bash -`
2. `sudo apt-get install -y nodejs`

## Install TiddlyWiki
for organisation and stuff

1. `sudo npm install -g tiddlywiki`
2. TODO - Get the edition for the index
  - It needs the multi-user plugin
  - I need to find a way to spawn new wikis for people

## Install Calibre
For ebooks. This may take some more poking than just installing calibre.
