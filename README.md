# Rat Pie Box

I would like as much of this as possible to be usable without needing any other
software on the phone or computer used to access it.

See `Instructions.md` for the current set of set-up instructions. It will
hopefully be expanded in the future.

## Things that are going to be on the Rat Pie Box

- Gitea - a light weight github like server
- TiddlyWiki - a personal wiki
- MySQL - used by gitea, general purpose relational database
- Node - Used by TiddlyWiki
- Calibre - an ebook library (instructions aren't finished yet)

TiddlyWiki is going to be used as an index that we can use to search and sort.

Gitea will be used to distribute software.

Calibre will be used for books and comics.


## Other things we may want

- A media server (mini dlna? OSMC?)
- A real FTP server? I am hoping to have a tiddlywiki index the available things
  and have links to download files. I am not sure if that is enough.
- A instant messaging option
- A forum thing
