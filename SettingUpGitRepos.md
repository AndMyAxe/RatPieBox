# Setting up Git repos

For each repo you want to have both in your Rat Pie Box git repo and in some
remote repo (like GitLab or GitHub) you have a few options.

If you want to have a copy of a remote repo on your Gitea server but you won't
modify it than you can mirror the remote repo. Migrate the repo and make sure
that the Mirror option is checked.

If you want to make your own copy that you can then modify and you aren't
concerned about getting new updates from the original repo than just migrate
the repo.

If you want to be able to get updates from a remote repo and make changes to
your repo as well than you can set up your Rat Pie server as the origin and the
other repo with another name. Then pull from the other repo and push to your
Rat Pie repo.

1. In a terminal navigate to the folder with your repo
2. Add the remote repo to you want like this:
  - `git remote add public path-to-repo`
  - Change `public` to whatever name you want, I am using `public` for publicly
  available repos.
  - Change `path-to-repo` to whatever the git uri
3. To pull changes to the remote repo to your Rat Pie repo
  - `git pull public`
  - `git push`
4. To push changes to the remote repo from your Rat Pie repo
  - `git pull`
  - `get push public`

I am planning on making scripts that will take care of this.
Also some git hooks to take care of some of this.

Setting up a git hook to push to the remote repo when you push to the Rat Pie
repo if there is an internet connection.

Note: For this you have to have the remote server in the Rat Pie Box's
known_hosts. The simplest way to do this is to ssh into the pi and push or
clone a repo to or from the server (ie github or gitlab), and say yes when it asks if you want to add the server to known_hosts.
