#!/bin/bash

# Set values below here

# Wireless SSID for your home network
# This si the wifi ssid and password for your home network if you are going to have
# the rat pie box connect to the internet
INTERNETSSID=PutYourSSIDHere
INTERNETPASSWORD=PutYourPasswordHere

# The email address associated with the rsa key for the git stuff
EMAIL=youremail@email.com
# The password used by the gitea user in the mysql database
MYSQLPASSWORD=SomePassword
# SSID for the wireless network (the one created by the rat pie box)
SSID=WirelessSSID
# Password for the access point (for the rat pie box access point)
WIRELESSPASSWORD=SomeWirelessPassword

# Password for to log into the Rat Pie Box as the gitea user. You will never
# need to do this.
GITEAUSERPASSWORD=GiteaUserPassword

# Don't edit anything below this line.

#==============================================================================#

# Things that aren't done: setting locale and password

# Add the harddrive to /etc/fstab if it isn't there
# Get the UUID of the drive first
UUID=$(blkid /dev/sda1 -o value -s UUID)
# Check if the line is in fstab, if not add it
fstab=/etc/fstab
if [[ $(grep -q "PieTin" "$fstab") ]]
then
  echo "# Add PieTin USB drive" >> /etc/fstab
  echo "UUID=$UUID /media/PieTin ext4 defaults 0 0"
else
  echo "Entry already in fstab."
fi

# Clear off unneeded stuff
sudo apt-get remove libreoffice*
sudo apt-get remove idle*
sudo apt-get remove epiphany*
sudo apt-get remove wolfram*
sudo apt-get remove scratch*
sudo apt-get remove minecraft-pi
sudo apt-get remove chromium-browser
sudo apt autoremove

# Install things needed for wifi shenanigans
sudo apt-get install dnsmasq hostapd

# make /etc/dnsmasq.conf
echo "interface=lo,ap0" > /etc/dnsmasq.conf
echo "no-dhcp-interface=lo,wlan0" >> /etc/dnsmasq.conf
echo "bind-interfaces" >> /etc/dnsmasq.conf
echo "server=8.8.8.8" >> /etc/dnsmasq.conf
echo "domain-needed" >> /etc/dnsmasq.conf
echo "bogus-priv" >> /etc/dnsmasq.conf
echo "dhcp-range=192.168.10.50,192.168.10.150,255.255.255.0,24h" >> /etc/dnsmasq.conf

# make /etc/hostapd/hostapd.conf
echo "ctrl_interface=/var/run/hostapd" > /etc/hostapd/hostapd.conf
echo "ctrl_interface_group=0" >> /etc/hostapd/hostapd.conf
echo "interface=ap0" >> /etc/hostapd/hostapd.conf
echo "driver=nl80211" >> /etc/hostapd/hostapd.conf
echo "ssid=$SSID" >> /etc/hostapd/hostapd.conf
echo "hw_mode=g" >> /etc/hostapd/hostapd.conf
echo "channel=7" >> /etc/hostapd/hostapd.conf
echo "wmm_enabled=0" >> /etc/hostapd/hostapd.conf
echo "macaddr_acl=0" >> /etc/hostapd/hostapd.conf
echo "auth_algs=1" >> /etc/hostapd/hostapd.conf
echo "wpa=2" >> /etc/hostapd/hostapd.conf
echo "wpa_passphrase=$WIRELESSPASSWORD" >> /etc/hostapd/hostapd.conf
echo "wpa_key_mgmt=WPA-PSK" >> /etc/hostapd/hostapd.conf
echo "wpa_pairwise=TKIP" >> /etc/hostapd/hostapd.conf
echo "rsn_pairwise=CCMP" >> /etc/hostapd/hostapd.conf

# In the file `/etc/default/hostapd` change the line `#DAEMON_CONF` to
# `DAEMON_CONF="/etc/hostapd/hostapd.conf"`
sed -i -e 's/#DAEMON_CONF/DAEMON_CONF="\/etc\/hostapd\/hostapd.conf"/g' /etc/default/hostapd

# set up /etc/wpa_supplicant/wpa_supplicant.conf
echo "ctrl_interface=DIR=/var/run/wpa_supplicant GROUP=netdev" > /etc/hostapd/hostapd.conf
echo "update_config=1" >> /etc/hostapd/hostapd.conf
echo "" >> /etc/hostapd/hostapd.conf
echo "network={" >> /etc/hostapd/hostapd.conf
echo "  ssid=\"$INTERNETSSID\"" >> /etc/hostapd/hostapd.conf
echo "  psk=\"$INTERNETPASSWORD\"" >> /etc/hostapd/hostapd.conf
echo "  id_str=\"AP1\"" >> /etc/hostapd/hostapd.conf
echo "}" >> /etc/hostapd/hostapd.conf

# Get the mac address for wlan0
# We may need to increment this by one for this next part. References are
# inconsistent about this.
MACADD=$(cat /sys/class/net/wlan0/address)

# Set up /etc/network/interfaces
echo "allow-hotplug wlan0" > /sys/class/net/wlan0/address
echo "iface wlan0 inet manual" >> /sys/class/net/wlan0/address
echo "  pre-up ifup ap0" >> /sys/class/net/wlan0/address
echo "  wpa-roam /etc/wpa_supplicant/wpa_supplicant.conf" >> /sys/class/net/wlan0/address
echo "  pre-down ifdown ap0" >> /sys/class/net/wlan0/address
echo "iface AP1 inet dhcp" >> /sys/class/net/wlan0/address
echo "" >> /sys/class/net/wlan0/address
echo "allow-hotplug ap0" >> /sys/class/net/wlan0/address
echo "iface ap0 inet static" >> /sys/class/net/wlan0/address
echo "  pre-up iw phy phy0 interface add ap0 type __ap0" >> /sys/class/net/wlan0/address
echo "  pre-up ip link set ap0 address $MACADD" >> /sys/class/net/wlan0/address
echo "  post-down iw dev ap0 del" >> /sys/class/net/wlan0/address
echo "  address 192.168.10.1" >> /sys/class/net/wlan0/address
echo "  netmask 255.255.255.0" >> /sys/class/net/wlan0/address
echo "  hostapd /etc/hostapd/hostapd.conf" >> /sys/class/net/wlan0/address

# Set up /etc/rc.local
echo "#!/bin/sh -e" > /etc/rc.local
echo "#" >> /etc/rc.local
echo "# rc.local" >> /etc/rc.local
echo "#" >> /etc/rc.local
echo "# This script is executed at the end of each multiuser runlevel." >> /etc/rc.local
echo "# Make sure that the script will "exit 0" on success or any other" >> /etc/rc.local
echo "# value on error." >> /etc/rc.local
echo "#" >> /etc/rc.local
echo "# In order to enable or disable this script just change the execution" >> /etc/rc.local
echo "# bits." >> /etc/rc.local
echo "#" >> /etc/rc.local
echo "# By default this script does nothing" >> /etc/rc.local
echo "" >> /etc/rc.local
echo "# Print the IP address" >> /etc/rc.local
echo "_IP=$(hostname -I) || true" >> /etc/rc.local
echo "if [ \"$_IP\" ]; then" >> /etc/rc.local
echo "  printf \"My IP address is %s\n\" \"$_IP\"" >> /etc/rc.local
echo "fi" >> /etc/rc.local
echo "" >> /etc/rc.local
echo "# Make the wireless interface to work consistently" >> /etc/rc.local
echo "# Wait for the boot to finish" >> /etc/rc.local
echo "sleep 5" >> /etc/rc.local
echo "# Reset the wlan interface. I don't know why this is necessary." >> /etc/rc.local
echo "sudo ifdown --force wlan0" >> /etc/rc.local
echo "sudo ifup wlan0" >> /etc/rc.local
echo "# start Gitea" >> /etc/rc.local
echo "su -c `/media/PieTin/Gitea/gitea web > /dev/null 2>&1 &` - gitea" >> /etc/rc.local
echo "# start TiddlyWiki" >> /etc/rc.local
echo "# Only uncomment this if you have tiddlywiki installed and setup" >> /etc/rc.local
echo "/media/PieTin/TiddlyWiki/start.sh &" >> /etc/rc.local
echo "# start the express server" >> /etc/rc.local
echo "node /media/PieTin/Server/express-server.js &" >> /etc/rc.local

# Add crontab thing if it doesn't exist
# This runs the script /media/PieTin/Server/resetwifi.sh every minute
if [[ $(crontab -l | grep "PieTin") ]]
then
  ( crontab -l ; echo "*/1 * * * * /bin/bash /media/PieTin/Server/resetwifi.sh >> /media/PieTin/errors.txt 2>&1" ) | crontab -
fi

# Make changes to /etc/avahi/avahi-daemon.conf, they may not be necessary
# Disable ipv6 announcing it was sometimes necessary according to the internets
sed -i -e 's/use-ipv6=yes/use-ipv6=no/g' /etc/default/hostapd

# Install mysql server
sudo apt-get install -y mysql-server

# Change the location fo the mysql database to the external drive
# https://stackoverflow.com/questions/1795176/how-to-change-mysql-data-directory
sudo /etc/init.d/mysql stop
sudo cp -R -p /var/lib/mysql /media/PieTin/MySQL
sudo sed -i -e "s/\/var\/lib\/mysql/\/media\/Pietin\/MySQL/g" /etc/mysql/mariadb.conf.d/50-server.cnf
sudo /etc/init.d/mysql restart

# Setup the Gitea user and stuff on the db
mysql -e "GRANT ALL PRIVILEGES ON *.* TO 'gitea'@'localhost' IDENTIFIED BY '$MYSQLPASSWORD';"
mysql -e "CREATE DATABASE gitea;"

# Setup and install Gitea
cd /media/PieTin
mkdir Gitea
cd Gitea
wget -O gitea https://dl.gitea.io/gitea/1.3.1/gitea-1.3.1-linux-arm-7
chmod +x gitea

# Add Gitea user
GITEAUSERPASS=$(perl -e 'print crypt($ARGV[0], "password")' $GITEAUSERPASSWORD)
sudo useradd -m -p $GITEAUSERPASS gitea

# Create the rsa key for the gitea user
su - gitea
ssh-keygen -t rsa -b 4096 -C "$EMAIL"
# Put the key into a file that is easy to see
cat ~/.ssh/id_rsa.pub > /media/PieTin/GiteaRSAPublicKey.txt

# Switch back to the pi user
su - pi

# Install Node
curl -sL https://deb.nodesource.com/setup_8.x | sudo -E bash -
sudo apt-get install -y nodejs

# Install TiddlyWiki
sudo npm install -g tiddlywiki

# Get the Rat Pie Box server
cd /media/PieTin
git clone https://gitlab.com/AndMyAxe/RatPieBoxServer.git
